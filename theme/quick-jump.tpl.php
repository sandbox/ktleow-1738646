<?php

/**
 * @file
 * Default theme implementation to display the basic Quick Jump overlay box.
 *
 * Variables:
 * - $form: Contains the entire rendered HTML form.
 */

?>
<div id="quick-jump">
  <?php print $form; ?>
</div>