Drupal.behaviors.quickJump = {
  attach: function(context, settings) {
    (function($) {

      // Hide submit button.
      $('#quick-jump .form-submit').hide();

      function showQj() {
        $('#quick-jump').fadeIn('fast', function() {
          $('#quick-jump .form-text:first').focus();
        });
      }
      function hideQj() {
        $('#quick-jump').fadeOut('fast', function() {
          $('#quick-jump .form-text:first').blur();
        });
      }

      // Bind hotkeys.
      $(document).bind('keydown', 'alt+q', function() {
        showQj();
      });
      $(document).bind('keydown', 'esc', function() {
        hideQj();
      });
      $('#quick-jump .form-text').bind('keydown', 'esc', function() {
        hideQj();
      });

      // Click anywhere else to close.
      $(document).click(function() {
        hideQj();
      });
      $('#quick-jump').click(function(e) {
        e.stopPropagation();
      });

      // Auto submit after selection.
    	$(document).ajaxComplete(quickJumpBindSubmit);
      function quickJumpBindSubmit() {
        $('#quick-jump .form-text:first').unbind('keypress').bind('keypress', function(e) {
    	    if (e.keyCode == 13) {
        	  $('#quick-jump .form-submit').click();
    	      return false;
    	    }
    	  });
        $('#quick-jump #autocomplete ul li').click(function() {
          $('#quick-jump .form-submit').click();
        });
    	}

    })(jQuery);
  }
}